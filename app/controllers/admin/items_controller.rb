# frozen_string_literal: true

class Admin::ItemsController < Admin::BaseController
  before_action :item, only: %i[show edit update destroy destroy_image]
  before_action :items, only: %i[index new]

  def index
  end

  def new
    @item = Item.new
  end

  def create
    @item = photo.items.new(item_params)
    if @item.save
      redirect_to admin_photo_items_path
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @item.update(item_params)
      redirect_to admin_photo_items_path(@photo)
    else
      render :edit
    end
  end

  def destroy
    flash[:error] = @item.errors.full_messages unless @item.destroy
    redirect_to admin_photo_items_path
  end

  def destroy_image
    image = ActiveStorage::Attachment.find_by(record_id: params[:id])
    image.purge
    redirect_to edit_admin_photo_item_path(@photo, @item)
  end

  private

  def item
    @item ||= photo.items.find(params[:id])
  end

  def items
    @items ||= photo.items
  end

  def photo
    @photo ||= Photo.find(params[:photo_id])
  end

  def item_params
    params.require(:item).permit(:name, :description, :image)
  end
end
