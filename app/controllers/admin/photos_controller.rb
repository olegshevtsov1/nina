# frozen_string_literal: true

class Admin::PhotosController < Admin::BaseController
  before_action :photo, only: %i[show edit update destroy destroy_image]

  def index
    @photos = Photo.all
  end

  def new
    @photo = Photo.new
  end

  def show
  end

  def edit
  end

  def create
    @photo = Photo.new(photo_params)
    if @photo.save
      redirect_to admin_photos_path
    else
      render :new
    end
  end

  def update
    if @photo.update(photo_params)
      redirect_to admin_photos_path
    else
      render :edit
    end
  end

  def destroy
    flash[:error] = @photo.errors.full_messages unless @photo.destroy
    redirect_to admin_photos_path
  end

  def destroy_image
    image = ActiveStorage::Attachment.find_by(record_id: params[:id])
    image.purge
    redirect_to edit_admin_photo_path(@photo)
  end

  private

  def photo
    @photo ||= Photo.find(params[:id])
  end

  def photo_params
    params.require(:photo).permit(:name, :description, :image)
  end
end
