# frozen_string_literal: true

class BaseController < ApplicationController
  layout 'frontend'
end
