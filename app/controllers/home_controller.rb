# frozen_string_literal: true

class HomeController < BaseController
  def show
    @photos = Photo.all
  end

  def about
  end
end
