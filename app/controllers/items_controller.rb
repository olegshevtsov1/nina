# frozen_string_literal: true

class ItemsController < BaseController
  before_action :items, only: %i[index]

  def index
  end

  private

  def items
    @items ||= photo.items
  end

  def photo
    @photo ||= Photo.find(params[:photo_id])
  end
end
