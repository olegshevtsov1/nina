# frozen_string_literal: true

class PhotosController < BaseController
  def index
    @photos = Photo.all
  end
end
