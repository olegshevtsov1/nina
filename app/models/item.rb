# frozen_string_literal: true

class Item < ApplicationRecord
  belongs_to :photo
  has_one_attached :image
  default_scope -> { order(:created_at) }
end
