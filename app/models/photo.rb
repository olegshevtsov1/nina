# frozen_string_literal: true

class Photo < ApplicationRecord
  has_many :items, dependent: :destroy
  has_one_attached :image
  validates :image, presence: true
  default_scope -> { order(:created_at) }
end
