server 'csd.up.km.ua', user: 'deployer', roles: %w{web app db}, port: 32020
set :branch, 'master'
set :rails_env, :production
set :stage , :production
set :sidekiq_env, :production
set :deploy_to, '/home/deployer/apps/nina.up.km.ua'
