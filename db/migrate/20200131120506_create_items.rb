class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :name, null: false, default: ""
      t.text :description, null: false, default: ""
      t.belongs_to :photo, index: true

      t.timestamps
    end
  end
end
